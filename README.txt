teaser.module
README.txt

Description
------------

The teaser module adds several field to a node to make the display of
overview pages more flexible. It uses hook_nodeapi() to do so. The
additional fields are

* teaser_title - Provide an alternative title for overview pages
* teaser_image - A thumbnail to be placed on overview pages
* teaser_link - an arbitrary URL for the "read more" link
* teaser_weight - to sort

Configuration
-------------
 
To use the alternate left/right placement of teaser images, define two
classes teaser-image-even and teaser-image-odd in your style sheet.
